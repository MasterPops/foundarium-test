<?php

namespace App\Providers;

use App\Models\Brand;
use App\Models\Car;
use App\Models\CarModel;
use App\Models\User;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to your application's "home" route.
     *
     * Typically, users are redirected here after authentication.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, and other route configuration.
     */
    public function boot(): void
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip());
        });

        $this->routes(function () {
            Route::middleware('api')
                ->prefix('api')
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->group(base_path('routes/web.php'));
        });

        Route::bind('user_id', function (int $userId, \Illuminate\Routing\Route $route) {
            return User::findOrFail($userId);
        });

        Route::bind('brand_id', function (int $brandId, \Illuminate\Routing\Route $route) {
            return Brand::findOrFail($brandId);
        });

        Route::bind('car_model_id', function (int $carModelId, \Illuminate\Routing\Route $route) {
            return CarModel::findOrFail($carModelId);
        });

        Route::bind('car_id', function (int $carId, \Illuminate\Routing\Route $route) {
            return Car::findOrFail($carId);
        });
    }
}
