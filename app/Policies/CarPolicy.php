<?php

namespace App\Policies;

use App\Models\Car;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class CarPolicy
{
    use HandlesAuthorization;
    /**
     * Create a new policy instance.
     */
    public function occupy(User $user, Car $car): Response
    {
        if ($car->user_id == $user->id) return Response::deny('Вы уже управляете этим автомобилем');
        if ($car->user_id) return Response::deny('Автомобиль занят!');
        if ($user->car) return Response::deny('Вы в другом автомобиле! Сначала покиньте автомобиль');

        return Response::allow();
    }

    public function toFree(User $user): Response
    {
        if ($user->car) return Response::allow();

        return Response::deny('В данный момент вы не управляете автомобилем!');
    }
}
