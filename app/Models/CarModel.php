<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\CarModel
 *
 * @property int $id
 * @property string $name
 * @property int $brand_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Brand $brand
 * @see CarModel::brand()
 *
 **/
class CarModel extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'brand_id',
    ];

    public function brand(): BelongsTo
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }
}
