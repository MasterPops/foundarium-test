<?php

namespace App\Http\Controllers;

use App\Http\Requests\BrandRequest;
use App\Http\Resources\BrandResource;
use App\Models\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index()
    {
        $brands = Brand::query()->paginate();

        return BrandResource::collection($brands);
    }

    public function create(BrandRequest $request)
    {
        $data = $request->only([
            'name',
        ]);

        Brand::create($data);

        return $this->index();
    }

    public function update(Brand $brand, BrandRequest $request)
    {
        $data = $request->only([
            'name',
        ]);

        $brand->update($data);

        return BrandResource::make($brand);
    }

    public function delete(Brand $brand)
    {
        $brand->delete();

        return $this->index();
    }
}
