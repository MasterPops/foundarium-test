<?php

namespace App\Http\Controllers;

use App\Http\Requests\CarModelRequest;
use App\Http\Requests\CarRequest;
use App\Http\Resources\CarModelResource;
use App\Http\Resources\CarResource;
use App\Http\Resources\UserResource;
use App\Models\Car;
use App\Models\CarModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CarController extends Controller
{
    public function index()
    {
        $cars = Car::query()->paginate();

        return CarResource::collection($cars);
    }

    public function create(CarRequest $request)
    {
        $data = $request->only([
            'car_model_id',
        ]);

        Car::create($data);

        return $this->index();
    }

    public function update(Car $car, CarRequest $request)
    {
        $data = $request->only([
            'car_model_id',
        ]);

        $car->update($data);

        return CarResource::make($car);
    }

    public function delete(Car $car)
    {
        $car->delete();

        return $this->index();
    }

    public function occupy(Car $car)
    {
        $car->user()->associate(Auth::user());
        $car->save();

        return CarResource::make($car);
    }

    public function toFree()
    {
        $car = Auth::user()->car;
        $car->user()->dissociate();
        $car->save();

        return $this->index();
    }
}
