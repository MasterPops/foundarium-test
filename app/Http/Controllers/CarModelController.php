<?php

namespace App\Http\Controllers;

use App\Http\Requests\CarModelRequest;
use App\Http\Resources\CarModelResource;
use App\Models\CarModel;
use Illuminate\Http\Request;

class CarModelController extends Controller
{
    public function index()
    {
        $carModels = CarModel::query()->paginate();

        return CarModelResource::collection($carModels);
    }

    public function create(CarModelRequest $request)
    {
        $data = $request->only([
            'name',
            'brand_id',
        ]);

        CarModel::create($data);

        return $this->index();
    }

    public function update(CarModel $carModel, CarModelRequest $request)
    {
        $data = $request->only([
            'name',
            'brand_id',
        ]);

        $carModel->update($data);

        return CarModelResource::make($carModel);
    }

    public function delete(CarModel $carModel)
    {
        $carModel->delete();

        return $this->index();
    }
}
