<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        $request->authenticate();

        $user = Auth::user();
        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'user' => UserResource::make($user),
        ]);
    }

    public function logout()
    {
        if (Auth::check()) {
            $token = Auth::user()->currentAccessToken();
            if ($token && $token->name === 'login_as_api') {
                DB::table('administrator_login_as_token')
                    ->where('login_as_token_id', $token->id)
                    ->delete();
            }
            $token->delete();
        }
        Auth::guard('web')->logout();

        return response()->json([
            'message' => 'Logged out'
        ]);
    }
}
