<?php

namespace App\Http\Resources;

use App\Models\Car;
use App\Models\Brand;
use App\Models\User;
use App\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

class CarResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'car';

    /**
     * The "data" collection wrapper that should be applied.
     *
     * @var string
     */
    public static $collectionWrap = 'cars';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof Car) {

            return [
                'id' => $this->resource->id,
                'car_model_id' => $this->resource->car_model_id,
                'car_model' => new CarModelResource($this->resource->carModel),
                'user_id' => $this->resource->user_id,
                'user' => new UserResource($this->resource->user),
            ];

        }

        return parent::toArray($request);
    }
}
