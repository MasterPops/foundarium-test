<?php

namespace App\Http\Resources\Json;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource as IlluminateJsonResource;

class JsonResource extends IlluminateJsonResource
{

    /**
     * Create a new resource instance.
     *
     * @param mixed ...$parameters
     * @return static
     */
    public static function make(...$parameters)
    {
        if (is_array($parameters) && count($parameters) > 0
            && property_exists(get_class($parameters[0]), 'resource')) {
            $resourceClass = get_class_vars(get_class($parameters[0]))['resource'];
            if (is_a($resourceClass, IlluminateJsonResource::class, true)) {
                return new $resourceClass($parameters[0]);
            }
        }
        return parent::make(...$parameters);
    }

    /**
     * Create a new anonymous resource collection.
     *
     * @param mixed $resource
     * @return AnonymousResourceCollection
     */
    public static function collection($resource): AnonymousResourceCollection
    {
        return tap(new AnonymousResourceCollection($resource, static::class), function ($collection) {
            if (property_exists(static::class, 'preserveKeys')) {
                $collection->preserveKeys = (new static([]))->preserveKeys === true;
            }
        });
    }

    /**
     * Add additional meta data to the resource response.
     *
     * @param array $data
     * @return static
     */
    public function additional(array $data)
    {
        $this->additional = array_merge($this->additional, $data);
        return $this;
    }

    /**
     * Add additional meta data to the resource response.
     *
     * @param bool $condition
     * @param array $data
     * @return static
     */
    public function additionalWhen(bool $condition, array $data)
    {
        if ($condition) $this->additional($data);
        return $this;
    }

    protected function asDateTime(?Carbon $date): ?string
    {
        if (!$date) return null;
        return $date->toDateTimeString();
    }

    protected function asDate(?Carbon $date): ?string
    {
        if (!$date) return null;
        return $date->toDateString();
    }

}
