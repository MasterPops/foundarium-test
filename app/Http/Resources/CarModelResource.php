<?php

namespace App\Http\Resources;

use App\Models\CarModel;
use App\Models\Brand;
use App\Models\User;
use App\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

class CarModelResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'car_model';

    /**
     * The "data" collection wrapper that should be applied.
     *
     * @var string
     */
    public static $collectionWrap = 'car_models';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof CarModel) {

            return [
                'id' => $this->resource->id,
                'name' => $this->resource->name,
                'brand_id' => $this->resource->Brand_id,
                'brand' => new BrandResource($this->resource->brand),
            ];

        }

        return parent::toArray($request);
    }
}
