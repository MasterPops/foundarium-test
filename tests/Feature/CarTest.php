<?php

namespace Feature;

use App\Models\Brand;
use App\Models\Car;
use App\Models\CarModel;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class CarTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     */

    /** @test */
    public function index_without_login(): void
    {
        $response = $this->get('/api/car');

        $response->assertStatus(401);
    }

    /** @test */
    public function create_without_login(): void
    {
        $user = User::factory(1)->make()->first();
        $user->save();
        $user->refresh();

        $carModel = CarModel::factory(1)
            ->make()
            ->first();

        $carModel->save();

        $carModel->refresh();

        $response = $this->post('/api/car', [
            'user_id' => $user->id,
            'car_model_id' => $carModel->id,
        ]);

        $response->assertStatus(401);
    }

    /** @test */
    public function update_without_login(): void
    {
        $user = User::factory(1)->make()->first();
        $user->save();
        $user->refresh();


        $carModel = CarModel::factory(1)
            ->make()
            ->first();

        $carModel->save();

        $carModel->refresh();

        $response = $this->patch('/api/car/' . $carModel->id, [
            'user_id' => $user->id,
            'car_model_id' => $carModel->id,

        ]);

        $response->assertStatus(401);
    }

    /** @test */
    public function delete_without_login(): void
    {
        $carModel = CarModel::factory(1)
            ->make()
            ->first();

        $carModel->save();

        $carModel->refresh();

        $response = $this->delete('/api/car-model/' . $carModel->id);

        $response->assertStatus(401);
    }

    /** @test */
    public function occupy_without_login(): void
    {
        $car = Car::factory(1)
            ->make()
            ->first();

        $car->save();
        $car->refresh();

        $response = $this->post('/api/car/' . $car->id . '/occupy');

        $response->assertStatus(401);
    }

    /** @test */
    public function to_free_without_login(): void
    {
        $response = $this->post('/api/car/to-free');

        $response->assertStatus(401);
    }

    /** @test */
    public function index_with_login(): void
    {
        $user = User::factory(1)->make()->first();

        Auth::login($user);
        $response = $this->get('/api/car-model');

        $response->assertStatus(200);
    }

    /** @test */
    public function create_with_login(): void
    {
        $user = User::factory(1)->make()->first();
        $user->save();
        $user->refresh();

        Auth::login($user);

        $carModel = CarModel::factory(1)
            ->make()
            ->first();

        $carModel->save();

        $carModel->refresh();

        $response = $this->post('/api/car', [
            'user_id' => $user->id,
            'car_model_id' => $carModel->id,
        ]);

        $response->assertStatus(200);
    }

    /** @test */
    public function update_with_login(): void
    {
        $user = User::factory(1)->make()->first();
        $user->save();
        $user->refresh();

        Auth::login($user);

        $carModel = CarModel::factory(1)
            ->make()
            ->first();

        $carModel->save();

        $carModel->refresh();

        $car = Car::factory(1)
            ->make()
            ->first();

        $car->save();
        $car->refresh();

        $response = $this->patch('/api/car/' . $car->id, [
            'user_id' => $user->id,
            'car_model_id' => $carModel->id,
        ]);

        $response->assertStatus(200);
    }

    /** @test */
    public function delete_with_login(): void
    {
        $user = User::factory(1)->make()->first();

        Auth::login($user);

        $car = Car::factory(1)
            ->make()
            ->first();

        $car->save();
        $car->refresh();

        $response = $this->delete('/api/car/' . $car->id);

        $response->assertStatus(200);
    }

    /** @test */
    public function occupy_free_with_login(): void
    {
        $user = User::factory(1)->make()->first();
        $user->save();
        $user->refresh();

        Auth::login($user);

        $car = Car::factory(1)
            ->make()
            ->first();

        $car->user_id = null;
        $car->save();
        $car->refresh();

        $response = $this->post('/api/car/' . $car->id . '/occupy');

        $response->assertStatus(200);
    }

    /** @test */
    public function occupy_busy_with_login(): void
    {
        $user = User::factory(1)->make()->first();
        $user->save();
        $user->refresh();

        Auth::login($user);

        $car = Car::factory(1)
            ->make()
            ->first();

        $car->save();
        $car->refresh();

        $response = $this->post('/api/car/' . $car->id . '/occupy');

        $response->assertStatus(403);
    }

    /** @test */
    public function to_free_busy_with_login(): void
    {
        $user = User::factory(1)->make()->first();
        $user->save();
        $user->refresh();

        Auth::login($user);

        $car = Car::factory(1)
            ->make()
            ->first();
        $car->user_id = $user->id;

        $car->save();
        $car->refresh();

        $response = $this->post('/api/car/to-free');

        $response->assertStatus(200);
    }

    /** @test */
    public function to_free_free_with_login(): void
    {
        $user = User::factory(1)->make()->first();
        $user->save();
        $user->refresh();

        Auth::login($user);

        $response = $this->post('/api/car/to-free');

        $response->assertStatus(403);
    }
}
