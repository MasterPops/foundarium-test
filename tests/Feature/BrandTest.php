<?php

namespace Tests\Feature;

use App\Models\Brand;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class BrandTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     */

    /** @test */
    public function index_without_login(): void
    {
        $response = $this->get('/api/brand');

        $response->assertStatus(401);
    }

    /** @test */
    public function create_without_login(): void
    {
        $response = $this->post('/api/brand', [
            'name' => 'test',
        ]);

        $response->assertStatus(401);
    }

    /** @test */
    public function update_without_login(): void
    {
        $brand = Brand::factory(1)
            ->make()
            ->first();

        $brand->save();

        $brand->refresh();

        $response = $this->patch('/api/brand/' . $brand->id, [
            'name' => 'test'
        ]);

        $response->assertStatus(401);
    }

    /** @test */
    public function delete_without_login(): void
    {
        $brand = Brand::factory(1)
            ->make()
            ->first();

        $brand->save();

        $brand->refresh();

        $response = $this->delete('/api/brand/' . $brand->id);

        $response->assertStatus(401);
    }

    /** @test */
    public function index_with_login(): void
    {
        $user = User::factory(1)->make()->first();

        Auth::login($user);
        $response = $this->get('/api/brand');

        $response->assertStatus(200);
    }

    /** @test */
    public function create_with_login(): void
    {
        $user = User::factory(1)->make()->first();

        Auth::login($user);

        $response = $this->post('/api/brand', [
            'name' => 'test'
        ]);

        $response->assertStatus(200);
    }

    /** @test */
    public function update_with_login(): void
    {
        $user = User::factory(1)->make()->first();

        Auth::login($user);

        $brand = Brand::factory(1)
            ->make()
            ->first();

        $brand->save();

        $brand->refresh();

        $response = $this->patch('/api/brand/' . $brand->id, [
            'name' => 'test'
        ]);

        $response->assertStatus(200);
    }

    /** @test */
    public function delete_with_login(): void
    {
        $user = User::factory(1)->make()->first();

        Auth::login($user);

        $brand = Brand::factory(1)
            ->make()
            ->first();

        $brand->save();

        $brand->refresh();

        $response = $this->delete('/api/brand/' . $brand->id);

        $response->assertStatus(200);
    }
}
