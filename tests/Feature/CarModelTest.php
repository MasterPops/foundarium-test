<?php

namespace Feature;

use App\Models\Brand;
use App\Models\CarModel;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class CarModelTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     */

    /** @test */
    public function index_without_login(): void
    {
        $response = $this->get('/api/car-model');

        $response->assertStatus(401);
    }

    /** @test */
    public function create_without_login(): void
    {
        $brand = Brand::factory(1)
            ->make()
            ->first();

        $brand->save();

        $response = $this->post('/api/car-model', [
            'name' => 'test',
            'brand_id' => $brand->id,
        ]);

        $response->assertStatus(401);
    }

    /** @test */
    public function update_without_login(): void
    {
        $carModel = CarModel::factory(1)
            ->make()
            ->first();

        $carModel->save();

        $carModel->refresh();

        $brand = Brand::factory(1)
            ->make()
            ->first();

        $brand->save();

        $response = $this->patch('/api/car-model/' . $carModel->id, [
            'name' => 'test',
            'brand_id' => $brand->id,

        ]);

        $response->assertStatus(401);
    }

    /** @test */
    public function delete_without_login(): void
    {
        $carModel = CarModel::factory(1)
            ->make()
            ->first();

        $carModel->save();

        $carModel->refresh();

        $response = $this->delete('/api/car-model/' . $carModel->id);

        $response->assertStatus(401);
    }

    /** @test */
    public function index_with_login(): void
    {
        $user = User::factory(1)->make()->first();

        Auth::login($user);
        $response = $this->get('/api/car-model');

        $response->assertStatus(200);
    }

    /** @test */
    public function create_with_login(): void
    {
        $user = User::factory(1)->make()->first();

        Auth::login($user);

        $brand = Brand::factory(1)
            ->make()
            ->first();

        $brand->save();

        $brand->refresh();

        $response = $this->post('/api/car-model', [
            'name' => 'test',
            'brand_id' => $brand->id,
        ]);

        $response->assertStatus(200);
    }

    /** @test */
    public function update_with_login(): void
    {
        $user = User::factory(1)->make()->first();

        Auth::login($user);

        $carModel = CarModel::factory(1)
            ->make()
            ->first();

        $carModel->save();

        $carModel->refresh();


        $brand = Brand::factory(1)
            ->make()
            ->first();

        $brand->save();

        $brand->refresh();

        $response = $this->patch('/api/car-model/' . $carModel->id, [
            'name' => 'test',
            'brand_id' => $brand->id,
        ]);

        $response->assertStatus(200);
    }

    /** @test */
    public function delete_with_login(): void
    {
        $user = User::factory(1)->make()->first();

        Auth::login($user);

        $carModel = CarModel::factory(1)
            ->make()
            ->first();

        $carModel->save();

        $carModel->refresh();

        $response = $this->delete('/api/car-model/' . $carModel->id);

        $response->assertStatus(200);
    }
}
