<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\CarController;
use App\Http\Controllers\CarModelController;
use App\Http\Controllers\BrandController;
use App\Models\Car;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::group(['middleware' => 'guest'], function () {
        Route::post('login', [AuthController::class, 'login']);
    });
    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::post('logout', [AuthController::class, 'logout']);

    });
});

Route::group(['middleware' => 'auth:sanctum'], function () {

    Route::group(['prefix' => 'brand'], function () {
        Route::get('/', [BrandController::class, 'index']);
        Route::post('/', [BrandController::class, 'create']);
        Route::patch('/{brand_id}', [BrandController::class, 'update'])
            ->where('brand_id', '\d+');
        Route::delete('/{brand_id}', [BrandController::class, 'delete'])
            ->where('brand_id', '\d+');
    });

    Route::group(['prefix' => 'car-model'], function () {
        Route::get('/', [CarModelController::class, 'index']);
        Route::post('/', [CarModelController::class, 'create']);
        Route::patch('/{car_model_id}', [CarModelController::class, 'update'])
            ->where('car_model_id', '\d+');
        Route::delete('/{car_model_id}', [CarModelController::class, 'delete'])
            ->where('car_model_id', '\d+');
    });

    Route::group(['prefix' => 'car'], function () {
        Route::get('/', [CarController::class, 'index']);
        Route::post('/', [CarController::class, 'create']);
        Route::post('/to-free', [CarController::class, 'toFree'])
            ->middleware('can:toFree,'. Car::class);
        Route::patch('/{car_id}', [CarController::class, 'update'])
            ->where('car_id', '\d+');
        Route::delete('/{car_id}', [CarController::class, 'delete'])
            ->where('car_id', '\d+');
        Route::post('/{car_id}/occupy', [CarController::class, 'occupy'])
            ->middleware('can:occupy,car_id')
            ->where('car_id', '\d+');
    });
});


